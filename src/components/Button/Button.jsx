import React from "react";
import "./Button.scss";

export default class Button extends React.Component {
    render() {
        const { backgroundColor, text, onClick } = this.props;

        return (
            <button
                className="button"
                style={{ backgroundColor }}
                onClick={onClick}

            >
                {text}
            </button>
        );
    }
}
